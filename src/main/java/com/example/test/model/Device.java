package com.example.test.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "device",  schema = "public")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Device {

    @Id
    @Column(name = "d_id")
    private int deviceId;
    @Column(name = "d_name")
    private String deviceName;
}
