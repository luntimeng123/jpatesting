package com.example.test.model.relationship.oneToone;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "id_card")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdCard {

    @Id
    @Column(name = "ic_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int Id;
    @Column(name = "ic_number")
    private String cardNumber;
    @OneToOne
    @JoinColumn(name = "human_id", referencedColumnName = "h_id")
    private Human human;
}
