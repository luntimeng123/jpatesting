package com.example.test.model.relationship.embeded;


import javax.persistence.Embeddable;

@Embeddable // បញ្ជាក់ថា class និងយកទៅបង្កបក្នុង another entity
public class ContactInfo {

    private String address;
    private long phone;
    private String email;
}
