package com.example.test.model.relationship.embeded;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table()
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Id
    @Column(name = "emp_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int empId;
    @Column(name = "emp_name")
    private int empName;

    // how to embed class in entity
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "address",
                    column = @Column(name = "emp_address")),
            @AttributeOverride(name = "phone",
                    column = @Column(name = "emp_phone")),
            @AttributeOverride(name = "email",
                    column = @Column(name = "emp_email"))
    })
    private ContactInfo contactInfo;
}
