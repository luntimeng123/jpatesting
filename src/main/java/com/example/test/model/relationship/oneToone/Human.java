package com.example.test.model.relationship.oneToone;


import com.example.test.model.relationship.manyTomany.University;
import com.example.test.model.relationship.oneTomany.Job;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="human")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Human {

    @Id
    @Column(name="h_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int humanId;
    @Column(name = "h_name")
    private String humanName;
    @Column(name = "h_age")
    private int age;

    @OneToMany(mappedBy = "human",cascade = CascadeType.ALL)
    private List<Job> jobs;

    @ManyToMany()
    @JoinTable(
            name = "university_mapper",
            joinColumns = @JoinColumn(name = "h_id"),
            inverseJoinColumns = @JoinColumn(name = "un_id")
    )
    private List<University> university;
}
