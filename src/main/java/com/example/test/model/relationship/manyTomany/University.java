package com.example.test.model.relationship.manyTomany;

import com.example.test.model.relationship.oneToone.Human;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "university")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class University {

    @Id
    @Column(name = "un_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int UnId;
    @Column(name = "un_name")
    private String UnName;

    @ManyToMany()
    @JoinTable(
            name = "humans_mapper",
            joinColumns = @JoinColumn(name = "un_id"),
            inverseJoinColumns = @JoinColumn(name = "h_id")
    )
    private List<Human> humans;
}
