package com.example.test.model.relationship.oneTomany;


import com.example.test.model.relationship.oneToone.Human;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "job")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Job {

    @Id
    @Column(name = "j_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int jobId;
    @Column(name = "j_name")
    private String jobName;

    @ManyToOne
    @JoinColumn(name = "human_id",referencedColumnName = "h_id")
    private Human human;
}
