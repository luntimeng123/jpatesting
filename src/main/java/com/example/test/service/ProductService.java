package com.example.test.service;


import com.example.test.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    public List<Product> getAllProducts();
    public Product getProductByName(String name);
    public Product getByProductId(long id);
    public boolean saveProduct(Product product);
//    public Product editProduct(long productId,Product product);
}
