package com.example.test.controller;


import com.example.test.model.Product;
import com.example.test.payload.request.ProductRequest;
import com.example.test.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class HomeRestController {

    @Autowired
    private ProductService productService;

    @GetMapping("/product")
    public List<Product> show(){
        return productService.getAllProducts();
    }

    @GetMapping("/product/{id}")
    public Product getProductById(@PathVariable long id){
        return productService.getByProductId(id);
    }

    @GetMapping("/product/p")
    public Product showByName(@RequestParam String name){
        return productService.getProductByName(name);
    }

    @PostMapping("/product/save")
    public boolean saveProduct(@RequestBody ProductRequest productRequest){
        System.out.println("productRequest = " + productRequest);
        Product product = new Product();
        product.setProductName(productRequest.getProductname());
        productService.saveProduct(product);
        return true;
    }

    @PutMapping("/product/edit/{id}")
    public Product editProduct(@PathVariable long id, @RequestBody ProductRequest productRequest){
        Product product = productService.getByProductId(id);
        product.setProductName(productRequest.getProductname());
        productService.saveProduct(product);
        return product;
    }

}
